gg.toast("Done by Crais 📝")

-- Bypass on startup functions

function BP()
gg.clearResults()
gg.searchNumber("1", gg.TYPE_AUTO, false, gg.SIGN_EQUAL, 0, 1.8446744E19)
gg.clearResults()
gg.toast("Bypass Done!")
end

function bypassHelp()
bypassHelpAlert = gg.alert("Bypass - Function to protect against crashes. \nWithout it, your PUBG with a propability of 90% will exit after you try to activate something. \nYou need to start bypass at the correct time, it must be started at the begin of Tencent & Lighspeed logo. \nProbably, while you're reading this, the game had time to load and you need to restart it again.", "Finish script \nand exit the game", "Back to selection")
if bypassHelpAlert == 1 then gg.processKill() os.exit() end
if bypassHelpAlert == 2 then startBypass() 
end
end

function startBypass()
gg.setVisible(false)
bypassAlert = gg.alert("Bypass Now?", "What is this?", "NO", "YES")
if bypassAlert == 3 then BP() end
if bypassAlert == 2 then end
if bypassAlert == 1 then bypassHelp() 
end
end

startBypass()

-- Menu

function Main()
menu = gg.choice({
"1. 👑 Combo 👑",
"2. 🛩 WallHack & Color Menu 🛬",
"3. 💺 Lobby Menu 💺",
"4. 🎮 Game Menu 🎮",
"5. 🔫 Bullet Menu 🔫",
"6. 🌃 Visual Menu 🌄",
"7. 📝 Changelog 📝",
'🔚 EXIT'},nil," ⚔️ Crais V0.8.0 ⚔️ ")
if menu == 1 then CB() end
if menu == 2 then WHCM() end
if menu == 3 then LM() end
if menu == 4 then GM() end
if menu == 5 then RFM() end
if menu == 6 then VM() end
if menu == 7 then CLOG() end
if menu == 8 then EXIT() end
XCGK = -1 
end

function CB()
BSK()
gg.clearResults()
LR()
gg.clearResults()
SC()
gg.clearResults()
RATN()
gg.clearResults()
gg.toast("Combo Successful Activated")
end

function WHCM()
IBM = gg.multiChoice({
"1.  🚁 SD 625 Wallhack                                     (Lobby)",
"2.  🚁 SD 625 Perfect Wallhack                             (Lobby)",
"3.  🚁 SD 625+ Perfect Wallhack                            (Lobby)",
"4.  🚁 SD 660 Wallhack                                     (Lobby)",
"5.  🚁 SD 835 Wallhack                                     (Lobby)",
"6.  🚁 SD 835+ Wallhack                                    (Lobby)",
"7.  🚁 SD 845 Wallhack                                     (Lobby)",
"8.  🚁 Vehicle Wallhack                                    (Lobby)",
"9.  🦀 Red 🦀                                              (Lobby)",
"10. 🐬 Blue 🐬                                             (Lobby)",
"11. 🍐 Green 🍐                                            (Lobby)",
"12. 🍋 Yellow 🍋                                           (Lobby)",
'🔙 Back'},nil," 🛫 Use at Lobby 🛬  ")
if IBM == nil then
else
if IBM[1] == true then WH625() end
if IBM[2] == true then WH625PW() end
if IBM[3] == true then WH625PLUS() end
if IBM[4] == true then WH660() end
if IBM[5] == true then WH835() end
if IBM[6] == true then WH835PLUS() end
if IBM[7] == true then WH845() end
if IBM[8] == true then VW() end
if IBM[9] == true then Red() end
if IBM[10] == true then Blue() end
if IBM[11] == true then Green() end
if IBM[12] == true then Yellow() end
if IBM[13] == true then Main() end
end
end

function WH625()
gg.clearResults()
gg.setRanges(gg.REGION_BAD)
gg.searchNumber("2.0F;4.7961574e21F;4.7408166e21F:512", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(20)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.searchNumber("2.0F;4.8699472e21F;4.8699466e21F:512", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(20)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.searchNumber("2.0F;4.7777152e21F;4.7777146e21F:512", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(20)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("SD625 Wallhack Activated.")
end

function WH625PW()
gg.setRanges(gg.REGION_BAD)
gg.searchNumber("4,140D;4.7408166e21;5.6896623e-29;4.7961574e21;3.7615819e-37;2:", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(3)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.searchNumber("200,866D;0.24022650719;0.69314718246;0.00999999978;1;-1;2;-127:", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(2)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.searchNumber("32,770D;0.01799999923;0.29907226562;-1;2:", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(4)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.searchNumber("50,331,648D;0.01799999923;0.29907226562;0.5869140625;0.11401367188;-1;2:", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("-1", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(2)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.searchNumber("50,331,648D;0.04000854492;0.11999511719;-0.02749633789;-0.57177734375;-1;2:", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("-1", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(1)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.searchNumber("266,400D;0.24022650719;0.69314718246;0.00999999978;1;-1;-127;2:", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(10)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.searchNumber("50,331,648D;0.04000854492;0.11999511719;-0.02749633789;-0.57177734375;-1;2:", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(2)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.searchNumber("4,140D;4.7408149e21;-5.5695588e-40;4.814603e21;3.7615819e-37;2:", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(20)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("SD625 Perfect Wallhack Activated")
end

function WH625PLUS()
gg.clearResults()
gg.setRanges(gg.REGION_BAD)
gg.searchNumber("4,140D;4.7408166e21;5.6896623e-29;4.7961574e21;3.7615819e-37;2:", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(3)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.searchNumber("200,866D;0.24022650719;0.69314718246;0.00999999978;1;-1;2;-127:", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(2)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.searchNumber("32,770D;0.01799999923;0.29907226562;-1;2:", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(4)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.searchNumber("50,331,648D;0.01799999923;0.29907226562;0.5869140625;0.11401367188;-1;2:", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("-1", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(2)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.searchNumber("50,331,648D;0.04000854492;0.11999511719;-0.02749633789;-0.57177734375;-1;2:", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("-1", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(1)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.searchNumber("266,400D;0.24022650719;0.69314718246;0.00999999978;1;-1;-127;2:", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(10)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.searchNumber("50,331,648D;0.04000854492;0.11999511719;-0.02749633789;-0.57177734375;-1;2:", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(2)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.searchNumber("4,140D;4.7408149e21;-5.5695588e-40;4.814603e21;3.7615819e-37;2:", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(20)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("SD625+ Perfect Wallhack Activated")
end

function WH660()
gg.clearResults()
gg.setRanges(gg.REGION_ANONYMOUS)
gg.searchNumber("8E;2.5;6.0255834e-44::150", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2.5", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(100)
gg.editAll("-999", gg.TYPE_FLOAT)
gg.setRanges(gg.REGION_BAD)
gg.clearResults()
gg.searchNumber("2.718519e-43F;3.7615819e-37F;2.0F;-1.0F;1.0F;-127.0F;0.00999999978F::200", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(30)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.searchNumber("5.8013756e-42F;-5.5695588e-40F;2.0F::100", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(30)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("SD660 Wallhack Activated")
end

function WH835()
gg.clearResults()
gg.setRanges(gg.REGION_BAD)
gg.searchNumber("5.1097599e21;2.0;1.6623071e-19;3.6734297e-39;1.66433e10::17", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(30)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.searchNumber("2.0;-1.0;0.0;1.0;-127.0::17", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(30)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("SD835 Wallhack Activated")
end

function WH835PLUS()
gg.setRanges(gg.REGION_BAD)
gg.searchNumber("5.8013756e-42F;-5.5695588e-40F;2.0F::100", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(30)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.setRanges(gg.REGION_BAD)
gg.searchNumber("2.718519e-43F;3.7615819e-37F;2.0F;-1.0F;-127.0F::520", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(20)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("SD835+ Wallhack Activated")
end

function WH845()
gg.clearResults()
gg.setRanges(gg.REGION_BAD)
gg.searchNumber("5.1097599e21;2.0", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(30)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.setRanges(gg.REGION_BAD)
gg.searchNumber("1.3912556e-19;1.1202057e-19;3.7615819e-37;2.0", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(30)
gg.editAll("120", gg.TYPE_FLOAT)
gg.toast("Processing")
gg.clearResults()
gg.setRanges(gg.REGION_BAD)
gg.searchNumber("1.3912525e-19;1.1202042e-19;3.7615819e-37;2.0", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(30)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.setRanges(gg.REGION_BAD)
gg.searchNumber("5.201992e21;2.25000452995;2.0", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(30)
gg.editAll("120", gg.TYPE_FLOAT)
gg.clearResults()
gg.setRanges(gg.REGION_BAD)
gg.searchNumber("6.50000333786;1.1202013e-19;3.7615819e-37;2::", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(1)
gg.editAll("99999", gg.TYPE_FLOAT)
gg.clearResults()
gg.setRanges(gg.REGION_BAD)
gg.searchNumber("9.8090893e-45;255;1;2::", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(1)
gg.editAll("99999", gg.TYPE_FLOAT)
gg.toast("SD845 Wallhack Activated")
end

function Red()
gg.clearResults()
gg.setRanges(gg.REGION_BAD)
gg.searchNumber("8,196D;8,192D;8,200D::", gg.TYPE_DWORD, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("8200", gg.TYPE_DWORD, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(10)
gg.editAll("7", gg.TYPE_DWORD)
gg.clearResults() 
gg.toast("Red Activated")
end

function Blue()
gg.clearResults()
gg.setRanges(gg.REGION_BAD)
gg.searchNumber("8200;1,080,035,591::512", gg.TYPE_DWORD, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("8200", gg.TYPE_DWORD, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(10)
gg.editAll("99", gg.TYPE_DWORD)
gg.toast("Blue Activated")
end

function Green()
gg.clearResults()
gg.setRanges(gg.REGION_BAD)
gg.searchNumber("32769;768;-2134900730", gg.TYPE_DWORD, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("32769", gg.TYPE_DWORD, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(10)
gg.editAll("32781", gg.TYPE_DWORD)
gg.clearResults()
gg.toast("Green Activated")
end

function Yellow()
gg.clearResults()
gg.setRanges(gg.REGION_BAD)
gg.searchNumber("8,192D;256D;65,540D;12D;8200D", gg.TYPE_DWORD, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("8200", gg.TYPE_DWORD, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(100)
gg.editAll("6", gg.TYPE_DWORD)
gg.clearResults()
gg.toast("Yellow Activated")
end

function LM()
IGM = gg.multiChoice({
"1.  🎯 No Recoil 🎯                                           【Once】",
"2.  🎯 Less Recoil 🎯                                         【Once】",
"3.  🔒 Mirco Aimbot 🔒                                        【Once】",
"4.  🔒 Medium Aimbot 🔒                                       【Once】",
"5.  🔒 Ultra Aimbot 🔒                                        【Once】",
"6.  🔒 Extreme Aimbot 🔒                                      【Once】",
"7.  🔒 AimLock 🔒                                             【Once】",
"8.  😈 Head Aimbot 😈                                         【Once】",
"9.  ⛄ Body White ⛄                                          【Once】",
"10. 🌚 Body Black 🌚                                          【Once】",
"11. ☠️ 50% Headshot MB ☠️                                     【Once】",
"12. 💀 90% Headshot MB 💀                                     【Once】",
"13. 💀 Head Size Mod 💀                                       【Once】",
"14. 📡 Antenna Always 📡                                      【Once】",
"15. ➕ Small Crosshair ➕                                     【Once】",
'🔙 Back'},nil," 💺 Lobby Menu 💺  ")
if IGM == nil then
else
if IGM[1] == true then OR() end
if IGM[2] == true then LR() end
if IGM[3] == true then MA() end
if IGM[4] == true then MedA() end
if IGM[5] == true then UA() end
if IGM[6] == true then EA() end
if IGM[7] == true then AL() end
if IGM[8] == true then HA() end
if IGM[9] == true then BW() end
if IGM[10] == true then BB() end
if IGM[11] == true then HM50() end
if IGM[12] == true then HM90() end
if IGM[13] == true then HSM() end
if IGM[14] == true then ATNS() end
if IGM[15] == true then SC() end
if IGM[16] == true then Main() end
end
end

function OR()
gg.setRanges(gg.REGION_ANONYMOUS)
gg.clearResults() gg.searchNumber('1868784978;1850305641;28518', gg.TYPE_DWORD, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber('1868784978', gg.TYPE_DWORD, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(1000)
gg.editAll('1868756421', gg.TYPE_DWORD)
gg.clearResults() gg.searchNumber('1750294898;1415932769;1819307365', gg.TYPE_DWORD, false, gg.SIGN_EQUAL, 0, -1) gg.searchNumber('1750294898;1415932769;1819307365', gg.TYPE_DWORD, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(1000)
gg.editAll('100000', gg.TYPE_DWORD)
gg.clearResults()
gg.toast("No Recoil Activated")
end

function LR()
gg.clearResults()
gg.setRanges(gg.REGION_ANONYMOUS) gg.searchNumber("1.4012985e-45;1;1;1;1;100000::100", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("1", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(100)
gg.editAll("0.5", gg.TYPE_FLOAT)
gg.toast("Less Recoil Activated")
end

function MA()
gg.setRanges(gg.REGION_ANONYMOUS)
gg.clearResults()
gg.searchNumber("3.5F;1F;1F;1F;200F;20F:512", gg.TYPE_DWORD, false)
gg.searchNumber("3.5", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, 0x7FFFFFFFFFFFFFFF)
gg.getResults(1995)
gg.editAll("-995", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("Micro AimBot Activeated")
end

function MedA()
gg.clearResults()
gg.setRanges(gg.REGION_ANONYMOUS)
gg.searchNumber("3.5F;1F;1F;1F;200F;20F:512", gg.TYPE_DWORD, false)
gg.searchNumber("200", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(850)
gg.editAll("9999", gg.TYPE_FLOAT)
gg.clearResults()
gg.setRanges(gg.REGION_ANONYMOUS)
gg.searchNumber("3.5F;1F;1F;1F;9999F;20F:512", gg.TYPE_DWORD, false)
gg.searchNumber("3.5", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(850)
gg.editAll("-9999", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("Medium AimBot Activated")
end
 
function UA()
gg.clearResults()
gg.searchNumber("999", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.clearResults()
gg.setRanges(gg.REGION_ANONYMOUS) gg.searchNumber("3.5;1;200;20::999", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("3.5;1;200;20", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(300)
gg.editAll("-1.0e10", gg.TYPE_FLOAT)
gg.clearResults()
gg.setRanges(gg.REGION_ANONYMOUS) gg.searchNumber("3.5;1;200;20::999", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1) gg.searchNumber("3.5;1;200;20::959", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(100)
gg.editAll("-9999999999", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("Ultra AimBot Activated")
end

function EA()
gg.searchNumber("999", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.clearResults()
gg.setRanges(gg.REGION_ANONYMOUS)
gg.clearResults()
gg.clearResults() gg.searchNumber("3.5;1;200;20::999", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("3.5;1;200;20", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(300)
gg.editAll("-1.0e10", gg.TYPE_FLOAT)
gg.setRanges(gg.REGION_ANONYMOUS)
gg.clearResults()
gg.clearResults()
gg.searchNumber("8;16;18", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("8;16;18", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(1000)
gg.editAll("65.5", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("Extreme AimBot Activated")
end

function AL()
gg.clearResults()
gg.setRanges(gg.REGION_BAD)
gg.searchNumber("-88.66608428955;26:512", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("26", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(800)
gg.editAll("460", gg.TYPE_FLOAT)
gg.clearResults()
gg.searchNumber("-88.73961639404;28:512", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("28", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(800)
gg.editAll("560", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("Aimlock Activated")
end

function HA()
gg.clearResults()
gg.setRanges(gg.REGION_ANONYMOUS)
gg.searchNumber("3.5;1;200;20::999", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("3.5;1;200;20", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(400)
gg.editAll("-1.0e10", gg.TYPE_FLOAT)
gg.setRanges(gg.REGION_ANONYMOUS)
gg.clearResults()
gg.toast("Head Aimbot Activated")
end

function BW()
gg.clearResults()
gg.setRanges(gg.REGION_ANONYMOUS) gg.searchNumber("0.05499718338;1.0", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(100)
gg.editAll("9999", gg.TYPE_FLOAT)
gg.toast("Body White Activated")
end

function BB()
gg.clearResults()
gg.setRanges(gg.REGION_ANONYMOUS) gg.searchNumber("0.05499718338;1.0", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(100)
gg.editAll("-9999", gg.TYPE_FLOAT)
gg.toast("Body Black Activated")
end

function HM50()
gg.clearResults()
gg.setRanges(gg.REGION_ANONYMOUS)
gg.searchNumber("9.201618;30.5;25", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("25", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(10)
gg.editAll("2000000", gg.TYPE_FLOAT)
gg.toast("50% Headshot Magic Bullet Activated")
end

function HM90()
gg.clearResults()
gg.setRanges(gg.REGION_BAD)
gg.searchNumber("-88.66608428955;26:512", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("26", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(2)
gg.editAll("460", gg.TYPE_FLOAT)
gg.clearResults()
gg.searchNumber("-88.73961639404;28:512", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("28", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(2)
gg.editAll("560", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("Headshot MB part 2...")
gg.setRanges(gg.REGION_ANONYMOUS)
gg.searchNumber("9.201618;30.5;25", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("25;30.5", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(10)
gg.editAll("200", gg.TYPE_FLOAT)
gg.clearResults()
gg.setRanges(gg.REGION_ANONYMOUS)
gg.searchNumber("9.201618;30.5;25", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("25;30.5", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(10)
gg.editAll("200", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("90% Headshot Magic Bullet Activated")
end

function HSM()
gg.clearResults()
gg.setRanges(gg.REGION_ANONYMOUS)
gg.searchNumber("9.201618;30.5;25", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("25;30.5", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(10)
gg.prompt({
[1] = 'Multiplier',
}, {
}, {
[1] = 'number',
}) 
gg.setValues({
[1] = { 
['address'] = 0xb2ac13fc,
['flags'] = 16, 
['original'] = '25.0',
['value'] = 75,
	},
[2] = { 
['address'] = 0xb2ac1400,
['flags'] = 16, 
['original'] = '30.5',
['value'] = 91.5,
	},
})
gg.editAll(200, gg.TYPE_FLOAT)
gg.toast("Head Size Mod Activated")
gg.clearResults()
gg.setVisible(false)
gg.clearResults()
end

function ATNS()
gg.setRanges(gg.REGION_ANONYMOUS)
gg.clearResults() gg.searchNumber("0.53446006775F;-1.68741035461F:501", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("-1.68741035461", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(1995)
gg.editAll("19995", gg.TYPE_FLOAT)
gg.clearResults()
gg.searchNumber("18.38612365723F;0.54026412964F:5", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("18.38612365723F;0.54026412964F:5", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(1995)
gg.editAll("19995", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("Antenna Always Activated")
end

function SC()
gg.setRanges(gg.REGION_ANONYMOUS)
gg.searchNumber("3.20000004768;1.09375", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("3.20000004768;1.09375", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(100)
gg.editAll("0", gg.TYPE_FLOAT)
gg.toast("Small Crosshair Actvated")
gg.clearResults()
end

function GM()
IVM = gg.multiChoice({
"1. 🏃 Running Antenna 🏃                                     【Landed/EveryMatch】",
"2. 🎒 Backpack Antenna 🎒                                    【Landed/EveryMatch】",
"3. 📡 All Antenna 📡                                         【Landed/EveryMatch】",
"4. 🔭 High Lvl Items Antenna 🔭                              【Landed/Once】",
"5. 🚙 Jeep Acceleration 🚙                                   【Landed/EveryMatch】",
"6. 🚙 Jeep In Water 🚙                                       【Landed/EveryMatch】",
"7. 🚗 Drive Through Wall 🚗                                  【Landed/EveryMatch】",
"8. 🐾 Hide Your Footsteps 🐾                                 【Landed/EveryMatch】",
'🔙 Back'},nil," 🎮 Game Menu 🎮")
if IVM == nil then
else
if IVM[1] == true then RATN() end
if IVM[2] == true then BATN() end
if IVM[3] == true then AATN() end
if IVM[4] == true then HLVLATN() end
if IVM[5] == true then JB() end
if IVM[6] == true then JPW() end
if IVM[7] == true then DTW() end
if IVM[8] == true then HFS() end
if IVM[9] == true then Main() end
end
end

function RATN()
gg.setRanges(gg.REGION_ANONYMOUS)
gg.setVisible(false)
gg.searchNumber("18.38613319397F;0.53447723389F;3.42665576935F", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("18.38613319397;0.53447723389;3.42665576935", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(3)
gg.editAll("26666", gg.TYPE_FLOAT)
gg.toast("Running Antenna Activated.")
end

function BATN()
gg.clearResults(850)
gg.searchNumber("0.9378669858F;1.0F;0.61365610361F::55", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("1", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(850)
gg.editAll("3500", gg.TYPE_FLOAT)
gg.toast("Backpack Antenna Activated")
end

function AATN()
gg.setRanges(gg.REGION_ANONYMOUS) gg.searchNumber("1.0;0.9537679553;0.06111789867", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("1", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(100)
gg.editAll("99999", gg.TYPE_FLOAT)
gg.setRanges(gg.REGION_ANONYMOUS)
gg.clearResults() gg.searchNumber("0.53446006775F;-1.68741035461F:501", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("-1.68741035461", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(1995)
gg.editAll("99999", gg.TYPE_FLOAT)
gg.clearResults()
gg.searchNumber("18.38613319397F;0.53447723389F;3.42665576935F",gg.TYPE_FLOAT,false,gg.SIGN_EQUAL,0,-1) gg.searchNumber("18.38613319397;0.53447723389;3.42665576935",16,false,gg.SIGN_EQUAL,0,-1)
gg.getResults(13)
gg.editAll("99999",gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("All Antenna Activated")
end

function HLVLATN()
gg.clearResults()
gg.setRanges(gg.REGION_BAD)
gg.searchNumber("7.1689529418945", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(3)
gg.editAll("999999999", gg.TYPE_FLOAT)
gg.clearResults()
gg.setRanges(gg.REGION_BAD)
gg.searchNumber("3.4779739379883;2.8345839977264;3.1967880725861;3.8841888904572;3.1528658866882::208", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("3.4779739379883", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(1)
gg.editAll("003,005,0", gg.TYPE_FLOAT)
gg.clearResults()
gg.setRanges(gg.REGION_BAD)
gg.searchNumber("7.4993133544922", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("7.4993133544922", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(1)
gg.editAll("999", gg.TYPE_FLOAT)
gg.clearResults()
gg.setRanges(gg.REGION_BAD)
gg.searchNumber("0.73620933294296", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(3)
gg.editAll("999999999", gg.TYPE_FLOAT)
gg.toast("High Level Items Antenna Activated")
end

function JB()
gg.clearResults()
gg.setRanges(gg.REGION_ANONYMOUS)
gg.searchNumber("0.647058857", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(10)
gg.editAll("-999", gg.TYPE_FLOAT)
gg.toast("Jeep Acceleration Activated")
end

function JPW()
gg.clearResults()
gg.searchNumber("150;85;45;-129;-85", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResultCount()
gg.searchNumber(45, gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResultCount()
gg.getResults(100)
gg.editAll("99996", gg.TYPE_FLOAT)
gg.toast("Jeep In Water Activated")
end

function DTW()
gg.clearResults()
gg.setRanges(gg.REGION_ANONYMOUS)
gg.searchNumber("982622900;1956496814;1103626239982622900;1956496814;1103626239::", gg.TYPE_DWORD, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("1956496814", gg.TYPE_DWORD, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(100)
gg.editAll("1091567616", gg.TYPE_DWORD)
gg.toast("Drive Through Wall Activated")
end

function HFS()
gg.toast("Hiding your Footsteps")
gg.clearResults()
gg.setRanges(gg.REGION_ANONYMOUS)
gg.searchNumber("2D;256D;256D;0.96666663885117;256D", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1) gg.searchNumber("0.96666663885117", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(3)
gg.editAll("999.9949", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("Hide Your Footsteps Activated")
end
  
function RFM()
RF = gg.multiChoice({
"1.  💥 SG Sniper Mode 💥                                     [After Pick Up SG/Do not use choke] ",
"2.  💥 M416 Fast Bullet 💥                                   [After Pick Up M416] ",
"3.  💥 Scar Fast Bullet 💥                                   [After Pick Up Scar] ",
"4.  💥 AKM Fast Bullet 💥                                    [After Pick Up AKM]",
"5.  💥 M16 Fast Bullet 💥                                    [After Pick Up M16] ",
"6.  💥 AWM Fast Bullet 💥                                    [After Pick Up AWM]",
"7.  💥 98K Fast Bullet 💥                                    [After Pick Up Kar98K]",
"8.  💥 M24 Fast Bullet 💥                                    [After Pick Up M24]",
"9.  💥 VSS Fast Bullet 💥                                    [After Pick Up VSS]",
"10. 💥 All SMG Fast Bullet 💥                                [After Pick Up Any SMG]",
"11. 💥 Fast Arrow 💥                                         [After Pick Up Crossbow] ",
"12. 💥 Mini Fast Bullet 💥                                   [After Pick Up Mini] ",
"13. 💥 SLR Fast Bullet 💥                                    [After Pick Up SLR] ",
"14. 💥 SKS Fast Bullet 💥                                    [After Pick Up SKS] ",
'🔙 Back'},nil," 🔫 Bullet Menu 🔫 ")
if RF == nil then
else
if RF[1] == true then SGSM() end
if RF[2] == true then M4RF() end
if RF[3] == true then SCRF() end
if RF[4] == true then AKMRF() end
if RF[5] == true then M16RF() end
if RF[6] == true then AWMRF() end
if RF[7] == true then JBKRF() end
if RF[8] == true then M24RF() end
if RF[9] == true then VSSRF() end
if RF[10] == true then CFQRF() end
if RF[11] == true then BOWRF() end
if RF[12] == true then MiniRF() end
if RF[13] == true then SLRF() end
if RF[14] == true then SKSRF() end
if RF[15] == true then Main() end
end
end

function SGSM()
gg.clearResults()
gg.setRanges(gg.REGION_ANONYMOUS)
gg.searchNumber("3.29999995232", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(20)
gg.editAll("0.01", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("Shotgun Sniper Mode Activated")
gg.setVisible(false)
gg.clearResults()
end

function M4RF()
gg.clearResults()
gg.setRanges(gg.REGION_ANONYMOUS)
gg.searchNumber("30D;10D;0F~1F;257D;3D::17", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("0.08600000292", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(50)
gg.editAll("0.04200000272", gg.TYPE_FLOAT)
gg.toast("M416 Fast Bullet Activated")
end

function SCRF()
gg.setRanges(gg.REGION_ANONYMOUS)
gg.searchNumber("30D;10D;0F~1F;257D;3D::17", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("0.09600000083", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(50)
gg.editAll("0.04800000022", gg.TYPE_FLOAT)
gg.toast("Scar Fast Bullet Activated")
end

function AKMRF()
gg.clearResults()
gg.setRanges(gg.REGION_ANONYMOUS)
gg.searchNumber("30D;10D;0F~1F;257D;3D::17", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("0.10000000149", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(50)
gg.editAll("0.001", gg.TYPE_FLOAT)
gg.toast("AKM Fast Bullet Activated")
end

function M16RF() 
gg.clearResults()
gg.setRanges (gg.REGION_ANONYMOUS) 
gg.searchNumber("90000", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(100)
gg.editAll("500000", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("M16 Fast Bullet Activated")
end

function AWMRF() 
gg.clearResults()
gg.setRanges (gg.REGION_ANONYMOUS) 
gg.searchNumber("91000", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(100)
gg.editAll("500000", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("AWM Fast Bullet Activated")
end

function JBKRF() 
gg.clearResults()
gg.setRanges (gg.REGION_ANONYMOUS) 
gg.searchNumber("76000", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(100)
gg.editAll("500000", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("Kar98K Fast Bullet Activated")
end

function M24RF() 
gg.clearResults()
gg.setRanges (gg.REGION_ANONYMOUS) 
gg.searchNumber("79000", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(100)
gg.editAll("500000", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("M24 Fast Bullet Activated")
end

function VSSRF()
gg.clearResults()
gg.setRanges (gg.REGION_ANONYMOUS) 
gg.searchNumber("33000", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(100)
gg.editAll("500000", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("VSS Fast Bullet Activated")
end

function CFQRF() 
gg.clearResults()
gg.setRanges (gg.REGION_ANONYMOUS) 
gg.searchNumber("40000", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(800)
gg.editAll("500000", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("All SMG Fast Bullet Activated")
end

function BOWRF() 
gg.clearResults()
gg.setRanges(gg.REGION_ANONYMOUS)
gg.searchNumber("16000", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(10)
gg.editAll("500000", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("Fast Arrow Activated")
end

function MiniRF()
gg.clearResults()
gg.setRanges(gg.REGION_ANONYMOUS)
gg.searchNumber("99000", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResultCount()
gg.searchNumber("99000", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResultCount()
gg.getResults(100)
gg.editAll("500000", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("Mini Fast Bullet Activated")
end

function SLRF()
gg.clearResults()
gg.setRanges(gg.REGION_ANONYMOUS)
gg.searchNumber("84000", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(10)
gg.editAll("1000000", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("SLR Fast Bullet Activated")
end

function SKSRF()
gg.clearResults()
gg.setRanges(gg.REGION_ANONYMOUS)
gg.searchNumber("80000", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(200)
gg.editAll("1000000", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("SKS Fast Bullet Activated")
end

function VM()
JM = gg.multiChoice({
"1. 🌑 Black Sky 🌑                                        【Landed/EveryMatch】",
"2. 🌿 No Grass Far 🌿                                     【Landed/EveryMatch】 ",
"3. 🌲 No Grass & Tree 🌲                                  【Landed/EveryMatch】",
"4. 🌴 No Grass & Tree Far 🌴                              【Landed/EveryMatch】",
"5. 🕴️ Player Size Menu 🕴️",       
'🔙 Back'},nil," 🌃 Visual Menu 🌄 ")
if JM == nil then
else
if JM[1] == true then BSK() end
if JM[2] == true then NG() end
if JM[3] == true then NGT() end
if JM[4] == true then NGTF() end
if JM[5] == true then CS() end
if JM[6] == true then Main() end
end
end

function BSK()
gg.clearResults()
gg.setRanges(gg.REGION_BAD)
gg.searchNumber("100F;1F;1,008,981,770D:99", gg.TYPE_FLOAT, false)
gg.searchNumber("100", gg.TYPE_FLOAT, false)
gg.getResults(100)
gg.editAll("-9999", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("Black Sky Activated")
end

function NG()
gg.clearResults()
gg.setRanges(gg.REGION_ANONYMOUS)
gg.searchNumber("8.0F;1.20000004768F;0.80000001192F;1.5F;0.80000001192F;1.5F::512", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("8.0", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(300)
gg.editAll("0", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("No Grass Activated")
end

function NGT()
gg.clearResults()
gg.setRanges(gg.REGION_BAD)
gg.searchNumber("2;10000", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(10)
gg.editAll("0", gg.TYPE_FLOAT, FREEZE_NORMAL)
gg.clearResults()
gg.setRanges(gg.REGION_BAD)
gg.searchNumber("4.8883906e21", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("4.8883906e21", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(100)
gg.editAll("4.8883906e20", gg.TYPE_FLOAT)
gg.clearResults()
gg.toast("No Grass + No Tree Near Activated")
end

function NGTF()
gg.clearResults()
gg.setRanges(gg.REGION_BAD)
gg.searchNumber("2;10000", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber("2", gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.getResults(10)
gg.editAll("0", gg.TYPE_FLOAT,FREEZE_NORMAL)
gg.clearResults()
gg.toast("No Grass + No Tree Far Activated")
end

function CS()
CS = gg.multiChoice({
"1. 🐜 Small Character 🐜                                   【Landed】",
"2. 🐉 Big Character 🐉                                     【Landed】",
'🔙 Back'},nil,'In Game/Landed')
if CS == nil then
else
if CS[1] == true then MC() end
if CS[2] == true then TC() end
if CS[3] == true then VM() end
end
end

function MC()
gg.clearResults()                                                                                                                                                     gg.searchNumber('3.0828566e-44;88;88;1;1;1', gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber('1', gg.TYPE_FLOAT, false, gg.SIGN_FLOAL, 0, -1)
gg.getResults(50)
gg.editAll('0.6', gg.TYPE_FLOAT)
gg.toast("Small Character Activated")
end

function TC()
gg.clearResults()                                                                                                                                                     gg.searchNumber('3.0828566e-44;88;88;1;1;1', gg.TYPE_FLOAT, false, gg.SIGN_EQUAL, 0, -1)
gg.searchNumber('1', gg.TYPE_FLOAT, false, gg.SIGN_FLOAL, 0, -1)
gg.getResults(50)
gg.editAll('0.6', gg.TYPE_FLOAT)
gg.toast("Big Character Activated")
end

function CLOG()
gg.alert("🔽🔽🔽🔽🔽🔽V0.8.0🔽🔽🔽🔽🔽🔽\n ➡️ Fixed All Codes\n🔼🔼🔼🔼🔼🔼V0.8.0🔼🔼🔼🔼🔼🔼\n\n🔽🔽🔽🔽🔽🔽V0.7.5🔽🔽🔽🔽🔽🔽\n ➡️ Added SKS Fast Bullet\n ➡️ Added SLR Fast Bullet\n ➡️ Added SG Sniper Mode\n ➡️ Added Headshot Size Mod\n🔼🔼🔼🔼🔼🔼V0.7.5🔼🔼🔼🔼🔼🔼")
Main()
end

function EXIT()
gg.toast(" ⚠️ Script Ended ⚠️ ")
print("Done by Crais 📝")
print("Thanks for using it 🎎")
os.exit()
end


while true do
if gg.isVisible(true) then
XCGK = 1
gg.setVisible(false)
end
if XCGK == 1 then
Main()
end
end